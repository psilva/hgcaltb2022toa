# HGCAL Testbeam 2022 Time-of-arrival studies

A set of ad-hoc scripts for plotting quanties related to ToA studiesn in the testbeam.

## Getting started

This repository holds a jupyter notebook to get started with the a simple analysis of the standalone simulation outputs.
The base ntuples were generated with the standalone tool (models 120-129) - [PFCALEE](https://github.com/pfs/PFCal/tree/master/PFCalEE). The standalone outputs were converted to a simple ntuple using this analyzer [HGCStandaloneAnalysis](https://gitlab.cern.ch/psilva/HGCStandaloneAnalysis).

The notebook is expected to run using SWAN. 
To get an automated start you either click here:

[![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/psilva/hgcaltb2022toa.git)

Alternatively do it by hand with the following steps

1. go to swan.cern 
1. open a terminal in the web page
1. clone this repository `git clone https://gitlab.cern.ch/psilva/hgcaltb2022toa.git'
1. open the `TB2022SimulationAnalysis.ipynb` notebook from the webpage
