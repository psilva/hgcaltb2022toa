import os
import ROOT
import json
from datetime import datetime
import numpy as np
import pandas as pd
from scipy import signal,stats
from scipy.optimize import curve_fit
from scipy.odr import ODR, Model, Data, RealData
from matplotlib.patches import Polygon,Circle
from matplotlib.collections import PatchCollection
from matplotlib.colors import Normalize
import matplotlib as mpl
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])
from scipy.ndimage import gaussian_filter1d

def setPadsInChargeInjFile(df,geometry):
    
    def _setPad(row):
        chip = int(row['chip'])
        channel = int(row['channel'])
        ch_type = int(row['channeltype'])
        half = int(np.floor(channel / 36))
        ch=channel
        if ch_type==0:
            ch=channel%36
        if ch_type==1:
            ch=36
        if ch_type==100:
            ch=37+channel%2
            half=1 if channel>1 else 0
        mask = (geometry['chip'] == chip) & (geometry['half']==half) 
        mask &= (geometry['channel']==ch) & (geometry['ch_type']==ch_type) 
        return geometry[mask].iloc[0]['pad']
 
    df['pad']=df.apply(_setPad,axis=1)
    return df
        
    

def build_geometry(url: str):
    
    """parses the geometry file"""
    
    geometry = []
    hexagons=json.load(open(url, "r"))
    for feature in hexagons['features']:
        prop = feature['properties']
        pad = prop['PAD_ID']
        chip = prop['Chip']
        channel = prop['Channel']
        ch_type = prop['Ch_type']
        half = int(np.floor(channel / 36))
        ch=channel
        if ch_type==0:
            ch=channel%36
        if ch_type==1:
            ch=36
        if ch_type==100:
            ch=37+channel%2
            half=1 if channel>1 else 0
        
        poly = np.array(feature['geometry']['coordinates'][0])
        x0 = poly[:, 0].mean()
        vx0=x0
        y0 = poly[:, 1].mean()
        vy0=y0
        
        #for visualization purposes shift calibration channels
        if ch_type==1:
            if chip==0 : vx0,vy0=-5,4
            if chip==1 : vx0,vy0=5,-4
            if chip==2 : vx0,vy0=-5,-4
            vy0+=-0.5*channel
            poly[:,0]+=vx0-x0
            poly[:,1]+=vy0-y0
        
        geometry.append([chip, half, ch, ch_type, pad, x0, y0, poly, vx0, vy0])

    return pd.DataFrame(geometry, columns=['chip', 'half', 'channel', 'ch_type', 'pad', 'x0', 'y0','poly', 'vx0','vy0'])


def convert_pad_to_channel(pad: int, geometry: pd.DataFrame):
    
    """converts a Si pad to a tuple  of (chip,half,ch,ch_type)"""

    mask = (geometry['pad'] == pad)
    if mask.sum() != 1 : return None
    return tuple(geometry[mask][['chip', 'half', 'channel', 'ch_type']].iloc[0].values.tolist())

def getNonIrradiatedChanels(pad,geo,rmin=1.):
    
    chip,half,ch,ch_type=convert_pad_to_channel(pad,geo)
    
    #derive the geometrical mask
    mask=(geo['pad']==pad)
    x0,y0=geo[mask][['x0','y0']].iloc[0]
    r=np.hypot(geo['x0']-x0,geo['y0']-y0)
    
    mask_r=(r>rmin) & (geo['chip']==chip) & (geo['half']==half) & (geo['pad']>0) & (geo['ch_type']==0)
    return geo[mask_r]['pad'].values.tolist()


def hexPlot(geo,z,outname,title='Irradiated pads',selpads=[],zlegs=None):

    """draws the hex plot with values and legends"""
    
    patches=[]
    fig,ax=plt.subplots(figsize=(8,8))
    for _,row in geo.iterrows():
        chip,half,ch,chtype=row[['chip','half','channel','ch_type']]
        pad=row['pad']
        if pad<0: continue
        patches.append( Polygon(row['poly'],closed=True))    
        if not pad in selpads: continue
        ax.text(row['vx0'],row['vy0'],f"{pad}",ha='center',va='center',fontsize=10)
        
        if not zlegs is None:
            if pad in zlegs:
                ax.text(row['vx0'],row['vy0']-0.2,zlegs[pad],ha='center',va='center',fontsize=8)
        else:
            ax.text(row['vx0'],row['vy0']-0.2,f"({chip:d},{half:d},{ch:d})",ha='center',va='center',fontsize=8)
        
    #draw the polygons
    patchColl=PatchCollection(patches)
    cnorm = Normalize(vmin=min(z),vmax=max(z))
    cmap = plt.get_cmap('Paired')
    colors=cmap( cnorm(z) )
    patchColl.set_color(colors)
    ax.add_collection(patchColl)

    ax.set_xlim(-5.5,5.5)
    ax.set_ylim(-5.5,5.5)
    ax.text(-5.4,5,title)
    plt.axis('off')
    labelplot()
    #fig.tight_layout()
    plt.savefig(outname)
    plt.close()
    
def labelplot(ax=None,loc=0):
    hep.cms.label('HGCAL beam test', data=True, ax=ax,loc=0,rlabel="Oct'22")

def selectChannelDataFromFile(f,chsel=None,usemt=True):
    
    """
    Selects raw data from a single file for a set of channels
    returns a pandas dataframe
    """

    #select uncorrupted channels
    if usemt: ROOT.EnableImplicitMT();
    df=ROOT.RDataFrame('unpacker_data/hgcroc',f)
    df=df.Filter('corruption==0') \
         .Define('ch_type','0 * (channel < 36) + 1 * (channel == 36) + 100 * (channel > 36)') \
         .Define('ch','(36 * half + channel) * (ch_type == 0) + half * (ch_type == 1) + (channel - 37 + half * 2) * (ch_type == 100)')
    
    #apply further channel selection
    if not chsel is None:
        mask=[]
        for (chip,half,ch,ch_type) in chsel:
            mask.append( f'(channel=={ch} && ch_type=={ch_type} && chip=={chip} && half=={half})')
        if len(mask)>0:
            mask='||'.join(mask)
            df=df.Filter(mask)        
        
    df=pd.DataFrame(df.AsNumpy())
    if usemt: ROOT.DisableImplicitMT()
    
    #return data
    return df

def selectChannelData(indata,chsel=None,nfiles=1,addCM=False):
    
    """
    Selects raw data from a single file or a directory specified by indata
    chsel=(channel,chip) can be further used to select a specific ROC channel
    returns a pandas dataframe
    """
    
    #build file list
    if '.root' in indata:
        flist=[indata]
    else:
        flist=[os.path.join(indata,r) for r in os.listdir(indata) if '.root' in r]
 
    #loop over files
    event_shift=0
    alldf=[]
    for i,f in enumerate(flist):
        if nfiles>0 and i>nfiles-1: break
        
        df=selectChannelDataFromFile(f,chsel)
        
        #add the needed common mode words to accompany the selected channels
        if addCM and not chsel is None:
            
            #build the list of all chips requested and compute the average common mode
            all_chips=set([c[0:2] for c in chsel])
            cm_chsel,cmsamehalf_chsel=[],[]
            for chip,half in all_chips:
                cm_chsel+=[(chip,0,37,100),(chip,0,38,100),(chip,1,37,100),(chip,1,38,100)]
                cmsamehalf_chsel+=[(chip,half,37,100),(chip,half,38,100)]
                
            cm_df=selectChannelDataFromFile(f,chsel=cm_chsel) 
            agg=cm_df.groupby(['event','chip']).agg(cmavg=('adc','mean'))
            agg.reset_index(inplace=True)
            del cm_df
            
            cm_df=selectChannelDataFromFile(f,chsel=cmsamehalf_chsel) 
            aggsamehalf=cm_df.groupby(['event','chip']).agg(cmshavg=('adc','mean'))
            aggsamehalf.reset_index(inplace=True)
            del cm_df
            
            #add the averages to the same chips
            df=df.merge(agg,on=['event','chip'],how='left')
            df=df.merge(aggsamehalf,on=['event','chip'],how='left')
        else:
            df['cmavg']=0.
            df['cmshavg']=0.
            
        df['event']+=event_shift
        event_shift=df['event'].max()+1
        alldf.append(df)
       
    #merge all and delete unused
    df=pd.concat(alldf)
    df.reset_index(inplace=True)
    df.drop(columns=['index','trigwidth','corruption','trigwidth'],inplace=True)
    for d in alldf: del d
    
    #return data
    return df


def getClosestRunInTime(run,runs_dir,ped_tag):

    """find the run closest in time"""

    pedruns=[d.replace(ped_tag,'') for d in os.listdir(runs_dir) if d.find(ped_tag)==0 and d.find('.h5')>0]
    pedruns=[datetime.strptime(d,'%Y%m%d_%H%M%S.h5') for d in pedruns]
    
    def _nearest(items=pedruns,
                 pivot=datetime.strptime(run,'run_%Y%m%d_%H%M%S')):
        return min(items, key=lambda x: abs(x - pivot))

    ped_time=_nearest()
    return ped_time.strftime('run_%Y%m%d_%H%M%S')

    

def pedestalAlgos(df,method=('adc','iq')):

    """
    pedestal-estimation routines
    df - dataframe
    method - method to apply is a tuple (variable,estimator)
        variable can be:
            adcm - use adc-1
            adc - use adc
        estimator can be:
            iq - median and associated quantiles
            mode - mode and stddev
    """
    
    var, est = method
    ped, sigma = np.zeros(df.shape[0]), np.zeros(df.shape[0])
    df_grouped=df.groupby(['chip','half','channel','ch_type'])
    
    if est=='iq':
        
        def _q(x,q=0.5):
            return x.quantile(q)

        agg=df_grouped.agg(q16 = (var, lambda x : _q(x,q=0.16)),
                           q50 = (var, lambda x : _q(x,q=0.50)),
                           avg = (var, 'mean'),
                           q84 = (var, lambda x : _q(x,q=0.84)))

        agg['pedm']=agg['q50']
        agg['ped']=agg['avg']
        agg['sigma']=0.5*(agg['q84']-agg['q16'])
        agg=agg.drop(columns=['q16','q50','q84'])
        
    elif est=='mode':        
        agg=df_grouped.agg(peak = (var,stats.mode),
                           stddev = (var,stats.tstd))
        agg['peak']=agg['peak'].apply(lambda x : x[0][0])
    
    agg.reset_index(inplace=True)    
    return agg


def runPedestalAlgos(df_ped):

    """
    runs the pedestal calibration routines and returns a summary dataset where the key is the channel id
    """
    
    agg_iq=pedestalAlgos(df_ped,method=('adc','iq'))
    agg_mode=pedestalAlgos(df_ped,method=('adc','mode'))
    return agg_iq.merge(agg_mode,on=['chip','half','channel','ch_type'])

    
def getPedestal(chsel,df_ped):
    
    """finds the values for pedestal subtraction in the dataframe"""
    chip,half,channel,ch_type=chsel
    mask = (df_ped['chip']==chip) & (df_ped['half']==half) & (df_ped['channel']==channel) & (df_ped['ch_type']==ch_type)
    ped,sigma=df_ped.loc[mask][['ped','sigma']].values[0]
    return ped,sigma


def determineCMcorrection(df,figname=None,title=None):
    
    
    #show full chip common mode and half chip common mode
    fig,ax=plt.subplots(1,2,figsize=(15,8))
    ped2=df['cmshavg'].median()
    ped4=df['cmavg'].median()
    mincm,maxcm=int(df['cmshavg'].min()-ped2),int(df['cmshavg'].max()-ped2)
    cmbins=np.linspace(mincm-0.5,maxcm-0.5,maxcm-mincm+1)
    std2=df['cmshavg'].std()
    std4=df['cmavg'].std()
    ax[0].hist(df['cmshavg']-ped2,bins=cmbins,label=rf'$CM_{{2}}$ std={std2:.2f}',histtype='step',lw=2)
    ax[0].hist(df['cmavg']-ped4,bins=cmbins,label=rf'$CM_{{4}} std={std4:.2f}$',histtype='step',lw=2)
    ax[0].set_xlabel(r'$CM-P_{CM}$')
    ax[0].set_ylabel('Events')
    ax[0].legend(fontsize=14)
    ax[0].grid()
    h2d=np.histogram2d(df['cmshavg']-ped2,df['cmavg']-ped4,bins=(cmbins,cmbins))
    hep.hist2dplot(*h2d, labels=False,cmin=1,ax=ax[1])
    ax[1].set_xlabel(r'$CM_{2}-P_{CM_2}$')
    ax[1].set_ylabel(r'$CM_{4}-P_{CM_4}$')
    ax[1].grid()
    labelplot(ax=ax[0])
    plt.savefig(figname.replace('.png','_2vs4.png'))
    plt.close()
        
    #use only the common mode sum and the ADC subtracting the pedestals
    prof=df[['cmavg','adc']].copy()
    ped_cm=prof['cmavg'].median()
    prof['cmavg']=prof['cmavg']-ped_cm
    prof['adc']=prof['adc']-prof['adc'].median()

    #profile
    min_cmavg=int(prof['cmavg'].quantile(0.01))
    max_cmavg=int(prof['cmavg'].quantile(0.99))
    n_cmavg=max_cmavg-min_cmavg+1
    cmbins=np.linspace(min_cmavg-0.5,max_cmavg-0.5,n_cmavg)
    prof['bin']=np.digitize(prof['cmavg'], bins=cmbins)
    agg=prof.groupby('bin').agg(cmavg_median=('cmavg','median'),cmavg_err=('cmavg','sem'),adc_median=('adc','median'),adc_err=('adc','sem'))
    
    #fit the model
    x,xerr,y,yerr=agg['cmavg_median'].values,agg['cmavg_err'].values,agg['adc_median'].values,agg['adc_err'].values

    def cmcorr_func(x,a,b):
        return x*a+b
    popt, pcov = curve_fit(cmcorr_func, x,y,sigma=yerr)

    rho,rhounc=popt[0],np.sqrt(pcov[0][0])
    off,offunc=popt[1],np.sqrt(pcov[1][1])
    report = {'rho':(rho,rhounc),
              'off':(off,offunc),
              'ped':ped_cm}
    
    if not figname is None:
        fig,ax=plt.subplots()
        ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none','color':'k'}
        ax.hist2d(prof['cmavg'],prof['adc'],bins=(cmbins,np.linspace(-15.5,14.5,31)),cmap=plt.get_cmap('Wistia'))
        ax.plot(x,cmcorr_func(x,*popt),color='red')
        ax.errorbar(x,y,xerr=xerr,yerr=yerr,**ebar_style)
        ax.set_xlabel('CM-CM$_{pedestal}$')
        ax.set_ylabel('ADC-ADC$_{pedestal}$')
        ax.text(0.05,0.95,title,transform=ax.transAxes)
        ax.text(0.05,0.9,rf'$\rho={rho:.3f}\pm{rhounc:.3f}$',transform=ax.transAxes,fontsize=16)
        ax.text(0.05,0.85,rf'offset$={off:.3f}\pm{offunc:.3f}$',transform=ax.transAxes,fontsize=16)
        plt.grid()
        labelplot()
        plt.savefig(figname)
        plt.close()
        
    return report

def defineTrigtimeCutByMaxADC(df,q=[0.1,1.0],maxreldiff=0.05,mineff=0.05,byMaxSpread=False,ped=-999999,sigma_ped=0):
    
    """
    This function projects the ADC for different trigtime requirements
    The optimal trigger time window is found by maximizing the ADC or its spread (byMaxSpread=True)
    All trigger times for which the values differ at most by maxreldiff of the optimal are considered
    A min. efficiency of the final selection is required (mineff)
    For reasonable ped/sigma_ped values, only adc values >ped+sigma_ped are considered by the algorithm
    """
    
    fulladcvstime = df[['trigtime','adc']].copy()
    
    #mask values which are within 2-sigma range of the pedestal
    mask_ped=(fulladcvstime['adc']>ped+2*sigma_ped)
    if mask_ped.sum()<10: 
        return None
    adcvstime=fulladcvstime[mask_ped].copy()
    
    #assign trigger time categories
    trigtime_ran=np.quantile(a=adcvstime['trigtime'], q=q).astype(int)
    trigtime_bins = np.linspace(trigtime_ran[0],trigtime_ran[1],trigtime_ran[1]-trigtime_ran[0]+1)
    adcvstime['tbin'] = np.digitize(adcvstime['trigtime'],trigtime_bins)
    
    #for each trigger bin find the median and an effective width of the ADC counts
    def _q_agg(x,qval):
        return x.quantile(qval)
    agg=adcvstime.groupby('tbin').agg( scale = ('adc',lambda x : 0.5*(_q_agg(x,0.84)-_q_agg(x,0.16)) ),
                                       loc = ('adc',lambda x : _q_agg(x,0.5) ),
                                       t = ('trigtime','mean'),
                                       n = ('adc','count'))

    #find the maximum and all trigtimes for which the average adc is within maxreldiff
    #if selection efficiency is too low, relax progressively the relative difference required
    maxloc=agg['scale'].max() if byMaxSpread else agg['loc'].max()
    mintrigtime,maxtrigtime=agg['t'].min(),agg['t'].max()
    eff=1
    if maxloc>0:
        while maxreldiff<1:            
            mask=np.abs(agg['scale']/maxloc-1)<maxreldiff if byMaxSpread else np.abs(agg['loc']/maxloc-1)<maxreldiff
            nsel=agg[mask]['n'].sum()
            eff=nsel/df.shape[0]
            if eff<mineff:
                maxreldiff = np.min([maxreldiff*1.5,1.])
                continue
            mintrigtime=agg[mask]['t'].min()
            maxtrigtime=agg[mask]['t'].max()
            break

    #if we converge to a single value, still allow the nearest neighbors
    if mintrigtime==maxtrigtime: 
        mintrigtime-=1
        maxtrigtime+=1
    
    #save also the full2D histogram
    #adc_ran=(fulladcvstime['adc'].min(),fulladcvstime['adc'].max()) 
    adc_ran = np.quantile(a=fulladcvstime['adc'], q=[0.005,0.995]).astype(int)
    adc_bins = np.linspace(adc_ran[0]-0.5,adc_ran[1]-0.5,adc_ran[1]-adc_ran[0]+1)
    adcvst_histo=np.histogram2d(fulladcvstime['trigtime'],fulladcvstime['adc'],
                                bins=(trigtime_bins,adc_bins))
    
    report = { 'trigtime':(mintrigtime,maxtrigtime),
               'loc':agg['loc'].values,
               'scale':agg['scale'].values,
               'x':agg['t'].values,
               'eff':eff,
               'cutoff':ped+sigma_ped,
               'maxreldiff':maxreldiff,
               'histo2d':adcvst_histo
             }                                  
    
    return report

def drawTrigtimeCutOptimByMaxADC(optim_report,outname):
    
    """draws the initial and final histograms obtained after the optimization of the trigtime optimization"""
    
    #only profile
    trigtime=optim_report['trigtime']
    plt.axvspan(trigtime[0]-0.1,trigtime[1]+0.1, alpha=0.6, color='gray')
    plt.text(0.5*(trigtime[0]+trigtime[1]),optim_report['loc'].max(),'IT',ha='center')
     
    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none','color':'k'}
    plt.errorbar(optim_report['x'],y=optim_report['loc'],yerr=optim_report['scale'],**ebar_style)
    plt.xlabel('Trigger time')
    plt.ylabel('Median ADC')
    plt.grid()
    labelplot()
    plt.savefig(outname)
    plt.close()
    
    #2D histogram
    cts2d,trigtime_bins,adc_bins=optim_report['histo2d']
    hep.hist2dplot(cts2d.copy(), trigtime_bins, adc_bins, labels=False,cmin=1);
    plt.errorbar(optim_report['x'],y=optim_report['loc'],yerr=optim_report['scale'],**ebar_style)

    cutoff=optim_report['cutoff']
    if cutoff>0:
        plt.plot([trigtime_bins[0],trigtime_bins[-1]],[cutoff,cutoff],ls='--',color='yellow')

    plt.xlabel('Trigger time')
    plt.ylabel('ADC counts')
    plt.grid()
    labelplot()
    plt.savefig(outname.replace('.png','_2d.png'))
    plt.close()

    #compare the PDF and CDF of the ADC spectra in different good trigger time bins
    mask=(trigtime_bins>=trigtime[0]) & (trigtime_bins<=trigtime[1])    
    fig,ax=plt.subplots(2,1,figsize=(15,8),sharex=True)
    overlay_style={'ls':'-','drawstyle':'steps','lw':2,'alpha':0.5}
    inc_pdf=None
    for i in range(trigtime_bins.shape[0]-1):

        if not mask[i] : continue
        
        #project and compute PDF and CDF
        pdf=cts2d[i,:]
        n=pdf.sum()
        if n==0: continue
            
        #sum up te inclusive distribution
        if inc_pdf is None: inc_pdf=np.zeros_like(pdf)
        inc_pdf+=pdf

        pdf=pdf/n
        cdf=np.cumsum(pdf)
        ax[0].plot(adc_bins, np.insert(pdf, 0, pdf[0]), **overlay_style)
        ax[1].plot(adc_bins, np.insert(cdf, 0, cdf[0]), **overlay_style, label=f'{trigtime_bins[i]:.0f}')

    #overlay the inclusive (final selection shape) as well
    if inc_pdf is None: return
    inc_pdf=inc_pdf/inc_pdf.sum()
    inc_cdf=np.cumsum(pdf)
    overlay_style['alpha']=1
    overlay_style['color']='k'
    overlay_style['lw']=3
    ax[0].plot(adc_bins, np.insert(pdf, 0, pdf[0]), **overlay_style)
    ax[1].plot(adc_bins, np.insert(cdf, 0, cdf[0]), **overlay_style,label='inclusive')
    
    for i in range(2):
        ax[i].set_ylabel('PDF' if i==0 else 'CDF')
        ax[i].grid()
    ax[1].set_xlabel('ADC counts')
    ax[1].legend(title='Trigger time',fontsize=14,loc='upper left',ncol=2)
    labelplot(ax=ax[0])    
    plt.savefig(outname.replace('.png','_proj.png'))
    plt.close()

    
def getBxm1Contribution(df,trigtime,title,outname,minleak=5/0.25):
    
    """profile ADC BX-1 versus ADC
    df - dataframe of selected events
    title - to label the plot
    trigtime - the range in which it's considered in-time
    outname - output name for the plots
    maxadcm - use only up to this value
    minleak - is the minimal ADC BX-1 to consider as important contribution to the next bunch
              e.g. 5/0.25 =at least 5 ADC in the next bunch for 25% leakage
    """

    mask_fit = (df['adcm']>minleak)
    if mask_fit.sum()<100:
        print(f'Warning: not enough data to derive BX-1 correction for minleak={minleak:.3f}')
        return {'rho':0,'dttime':0}
    
    #make a local copy for our purposes
    df=df[mask_fit].copy()
    df.reset_index(inplace=True)
    
    #assign the quantization bins from the start
    adcm_bins=np.quantile(a=df['adcm'], q=np.linspace(0,1,25))   
    df['adcmbin'] = np.digitize(df['adcm'],adcm_bins)
    adc_bins=np.quantile(a=df['adc'], q=np.linspace(0.01,0.99,25))   

    kcorrvals=[]

    #scan the [-25,-13] ns ~ [-32,-16] trigtime units range
    # each trigtime unit is 25 / 32 = 0.8 ns
    fig,ax = plt.subplots(4,4,figsize=(20,16),sharex=True,sharey=True)
    plt.subplots_adjust(wspace=0, hspace=0)
    plt.tick_params(axis='x', which='minor')
    for t in range(16,32):
        
        probe_trigtime=0.5*(trigtime[1]+trigtime[0])-t   
        delta_t_ns = -t*0.8

        i,j=int((t-16)/4),t%4
        ax[i][j].text(0.1,0.9,f'trigtime={probe_trigtime:.0f}',ha='left',fontsize=14,transform=ax[i][j].transAxes)
        ax[i][j].text(0.1,0.82,rf'$\Delta t={delta_t_ns:.1f} ns$',ha='left',fontsize=14,transform=ax[i][j].transAxes)
        
        if i==3: ax[i][j].set_xlabel(r'$ADC_{BX-1}$')
        if j==0: ax[i][j].set_ylabel('ADC')
        ax[i][j].set_xscale('log')
        ax[i][j].set_yscale('log')
        ax[i][j].grid()

        mask_ttime = (df['trigtime']==probe_trigtime)
        if mask_ttime.sum()<100: 
            ax[i][j].text(0.1,0.74,f'insuff. data',ha='left',fontsize=14,transform=ax[i][j].transAxes)
            continue
                  
        #profile the median ADC as function of ADC BX-1 for this bin
        #fit the model using an ordinary least square
        def _q_agg(x,qval):
            return x.quantile(qval)
        agg=df[mask_ttime].groupby('adcmbin').agg( adc_loc = ('adc',lambda x : _q_agg(x,0.5) ),
                                                   adc_scale = ('adc',lambda x : 0.5*(_q_agg(x,0.84)-_q_agg(x,0.16)) ),
                                                   adcm_loc = ('adcm',lambda x : _q_agg(x,0.5) ),
                                                   adcm_scale = ('adcm',lambda x : 0.5*(_q_agg(x,0.84)-_q_agg(x,0.16)) ),
                                                   n =('adc','count')
                                                   )
        agg=agg[agg['n']>5] #at least 5 counts per bin
        x,xerr,y,yerr=agg['adcm_loc'].values,agg['adcm_scale'].values,agg['adc_loc'].values,agg['adc_scale'].values
        beta0=(y[-1]-y[0])/(x[-1]-x[0])
        def _bxm1_func(x,a):
            return x*a
        fitdata = RealData(x, y, sx=xerr, sy=yerr)
        fitmodel = Model(_bxm1_func)
        odr = ODR(fitdata, fitmodel, beta0=[beta0])
        odr.set_job(fit_type=0)
        fit_output = odr.run()
        kcorr,kcorr_unc=fit_output.beta[0],fit_output.sd_beta[0]
        kcorrvals.append( [delta_t_ns,kcorr,kcorr_unc] )
        
        #display fit results, profile and 2D histogram
        ax[i][j].hist2d(df[mask_ttime]['adcm'],df[mask_ttime]['adc'],bins=(adcm_bins,adc_bins),cmap='Wistia',cmin=1)
        ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none','color':'k'}
        ax[i][j].errorbar(x,y=y,xerr=xerr,yerr=yerr,**ebar_style)
        xlin=np.linspace(x[0],x[-1],100)
        ax[i][j].plot(xlin,_bxm1_func(xlin,kcorr),ls='--',color='red',lw=2)
        ax[i][j].text(0.1,0.74,
                      rf'$\kappa_{{BX-1}}={kcorr:.3f}\pm{kcorr_unc:.3f}$',
                      ha='left',fontsize=14,transform=ax[i][j].transAxes)
    
    hep.cms.label('HGCAL beam test', data=True, ax=ax[0][0],loc=0,rlabel="")
    ax[0][3].text(1,1,"Oct'22",ha='right',va='bottom',transform=ax[0][3].transAxes)
    plt.savefig(outname.replace('.png','_fits.png'))
    plt.close()
    
    
    #leak model ~ integrate a CR-RC model
    kcorrvals=np.array(kcorrvals)
    x,y,yerr=kcorrvals[:,0],kcorrvals[:,1],kcorrvals[:,2]
    def _leak_model(x,tau,x0):
        dx=x-x0
        return 1.-np.exp(-dx/tau)*(1+dx/tau)
    popt, pcov = curve_fit(_leak_model, x, y, sigma=yerr,bounds=((1e-3,-50),(50,-10)))
    tau,tau_unc=popt[0],np.sqrt(pcov[0][0])
    t0,t0_unc=popt[1],np.sqrt(pcov[1][1])
    
    #compute the average correction for a uniform distribution of trigtimes
    #this has an analytical expression but at this point i have other things to do
    xlin=np.linspace(t0,t0+25,250)
    rhobxm1 = (_leak_model(xlin,*popt)).mean()
    
    fig,ax=plt.subplots()
    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none','color':'k'}
    ax.errorbar(x,y=y,yerr=yerr,**ebar_style)
    xlin=np.linspace(t0,t0+30,100)
    ax.plot(xlin,_leak_model(xlin,*popt),ls='--',color='red')
    ax.text(0.95,0.28,title,ha='right',transform=ax.transAxes)
    ax.text(0.95,0.22,rf'$\tau={tau:.1f}\pm{tau_unc:.1f}$',ha='right',transform=ax.transAxes)
    ax.text(0.95,0.16,rf'$t_{0}={t0:.1f}\pm{t0_unc:.1f}$',ha='right',transform=ax.transAxes)
    ax.text(0.95,0.08,rf'$<ADC/ADC_{{BX-1}}>={rhobxm1:.3f}$',ha='right',transform=ax.transAxes)
    
    ax.set_ylabel(r'$ADC / ADC_{BX-1}$')
    ax.set_xlabel(r'Time [ns]')
    ax.grid()
    labelplot()
    plt.savefig(outname)
    plt.close()
        
    report={'rho':rhobxm1,'tau':(tau,tau_unc),'dttime':0}
    del df
    return report


def compareSpectra( counts, labels, bins, title, oname, xlim=(-10.5,50.5)):

   
    fig,ax=plt.subplots()
    overlay_style={'ls':'-','drawstyle':'steps','lw':3}
    for i in range(len(counts)):
        ax.plot(bins, np.insert(counts[i], 0, counts[i][0]), label=labels[i], **overlay_style)
       
    ax.set_ylabel('Counts')
    ax.set_xlabel('ADC counts')
    ax.set_xlim(*xlim)
    ax.text(0.98,0.95,title,transform=ax.transAxes,ha='right')
    ax.grid()
    ax.legend(loc='upper left')
    labelplot()
    plt.savefig(oname)
    plt.close()
    
    
def analyzeFitResultStability(params,title,outname):
    
    syst_report={}
    
    x=params['trigtime']

    fig,ax=plt.subplots(4,1,figsize=(15,12),sharex=True)
    ebar_style={'marker':'s','elinewidth':1,'capsize':1,'ls':'none','color':'darkgray'}
    inc_ebar_style={'marker':'o','elinewidth':2,'capsize':2,'ls':'none','color':'k'}
    for i,(p,pt) in enumerate([('n_mu','Pedestal (res)'),('s_mpv','MIP'),('n_sigma','Noise'),('s2n','S/N')]):
        
        y=params[p].values[1:]
        yerr=params[p+'_unc'].values[1:]

        mask=(yerr/y<0.5)
        if mask.sum()==0:        
            continue
        systval=np.ptp(y[mask])/2 #max-min / peak-to-peak
        syst_report[p]=systval

        ax[i].fill_between(x[1:],np.ones_like(y)*y.min(),np.ones_like(y)*y.max(),color='yellow',alpha=0.8)      
        ax[i].errorbar(x[1:],y=y,yerr=yerr,**ebar_style)
        
        ax[i].text(0.05,0.95,rf'$\delta${pt}={systval:.2f}',horizontalalignment='left',verticalalignment='top', transform=ax[i].transAxes,fontsize=18)

        #overlay inclusive measurement
        y=params[p].values[0:1]
        yerr=params[p+'_unc'].values[0:1]
        ytoterr=np.hypot(yerr,[systval])
        ax[i].bar(x[0:1]+0.1, height=2*ytoterr, bottom=y-ytoterr, width=0.05, align='center', zorder=1, color='lightgray')
        ax[i].errorbar(x[0:1]+0.1,y=y,yerr=yerr,**inc_ebar_style)

        ax[i].set_ylim(y[0]-2*ytoterr[0],y[0]+2*ytoterr[0])
        ax[i].set_ylabel(pt)
        ax[i].grid()
    
    ax[0].text(0.95,0.95,title,horizontalalignment='right',verticalalignment='top', transform=ax[0].transAxes,fontsize=18)
    ax[3].set_xlabel('Trigger time')
    labelplot(ax=ax[0])
    plt.tight_layout()
    plt.savefig(outname)
    plt.close()
    
    return syst_report

