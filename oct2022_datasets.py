import pandas as pd

data_pions={
    80:{'dir':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pion_beam_150_80fC/beam_run/',
        'chargeinj':'/eos/cms/store/cmst3/group/hgcal/TBOct22/injection_scans/merged_adc_calib_80fC.h5',
        'pedestals':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pedestals/pedestal_80fC/pedestal_run/',
        'beam':[ 
               ]
       },
    160:{'dir':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pion_beam_150_160fC/beam_run/',
         'chargeinj':'/eos/cms/store/cmst3/group/hgcal/TBOct22/injection_scans/merged_adc_calib_160fC.h5',
         'pedestals':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pedestals/pedestal_160fC/pedestal_run/',
         'beam':[ 
                ]
       },
    320:{'dir':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pion_beam_150_320fC/beam_run/',
         'chargeinj':'/eos/cms/store/cmst3/group/hgcal/TBOct22/injection_scans/merged_adc_calib_320fC.h5',
         'pedestals':'/eos/cms/store/group/dpg_hgcal/tb_hgcal/2022/sps_oct2022/pedestals/pedestal_320fC/pedestal_run/',
         'beam':[ 
               ]
       },
    
}

def fillDataDict(url='pion_beams_sps_oct2022.csv'):
    df=pd.read_csv(url,sep='\t')
    df=df.astype({'pad':int,'gain':int,'E':int})
    for gain,group in df.groupby('gain'):
        data_pions[gain]['beam']=group.to_dict('record')
    del df
    return data_pions