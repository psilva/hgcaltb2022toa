#ifndef __TB2022SimulationTools_h__
#define __TB2022SimulationTools_h__

#include "ROOT/RVec.hxx"

using namespace ROOT::VecOps; 
using rvec_f = RVec<float>; 
using rvec_i = RVec<int>; 
using rvec_b = RVec<bool>; 

rvec_i getSortedHitsIndices(const rvec_f &q) {
    
    /**
    @short returns the sorted hits indices as a vector of integers
    */
    
    auto idx_s = Argsort(q, [](double a, double b) {return a > b;});
    rvec_i idx(idx_s.size());
    for(size_t i=0; i<idx_s.size(); i++) idx[i]=(int) idx_s[i];
    return idx;
}

float computeContainment(const rvec_f &x,const rvec_f &y,const rvec_f &q, const rvec_i &idx, float cl=0.9,float x0=0.,float y0=0.) {
    
    /**
    @short find the minimal radius containing a "cl" fraction of the total energy
    */
    
    if(q.size()==0) return 0.;
    float total_q = Sum(q);
    if(total_q<=0) return 0.;
    
    float thr(cl*total_q);
        
    //find the largest radius which capture cl*total_q of the energy
    float max_rho2(0.);
    float cumul_q(0.);
    for(auto i : idx) {        
        cumul_q += q[i];
        float rho2 = pow(x[i]-x0,2)+pow(y[i]-y0,2);
        if(rho2>max_rho2) max_rho2=rho2;
        if(cumul_q>=thr) break;
    }
    
    return sqrt(max_rho2);
}
 
#endif
